﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileOperations : MonoBehaviour {

	private GameObject GlobalScriptObject;
	private GlobalScript GlobalScript;
	private InitializeGameElements InitializeGameElementsScript;
	private PlayerActions PlayerActionsScript;
	private TileOperations TileOperationsScript;
    
    [HideInInspector] public List<GameObject> ExcludedTiles;
    [HideInInspector] public List<GameObject> PlayerMoveTiles;

	public class NeighborTile {
        public GameObject tile;
        public int distance;
        public NeighborTile cameFrom;
    }
    public Material HoverMaterial;
    public Material DefaultTileMaterial;
    public Material AvailableTileMaterial;
	public Material PlayerTileMaterial;

	void Awake () {
		GameObject GlobalScriptObject = GameObject.FindGameObjectWithTag("GlobalScript");
		GlobalScript = GlobalScriptObject.GetComponent<GlobalScript>();
        InitializeGameElementsScript = GlobalScriptObject.GetComponentInChildren<InitializeGameElements>();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    int GetCharacterMovement () {

        if(InitializeGameElementsScript.SelectedCharacter.tag == "Player") {
            return InitializeGameElementsScript.SelectedCharacter.GetComponent<PlayerActions>().Movement;
        } else {
            return InitializeGameElementsScript.SelectedCharacter.GetComponent<EnemyActions>().Movement;
        }

    }

	int CalculateDistance (GameObject tile, GameObject hoverTile) {

        int hoverTileRowIndex = hoverTile.GetComponent<TileActions>().rowIndex;
        int hoverTileColumnIndex = hoverTile.GetComponent<TileActions>().columnIndex;
        int currentTileRowIndex = tile.GetComponent<TileActions>().rowIndex;
        int currentTileColumnIndex = tile.GetComponent<TileActions>().columnIndex;

        return Mathf.Abs (currentTileRowIndex - hoverTileRowIndex) + Mathf.Abs (currentTileColumnIndex - hoverTileColumnIndex);
    }

    int FindLowestDistance (List<NeighborTile> nextTiles, int lowestDistance) {

        for (int a = 0; a < nextTiles.Count; a++) {
            if (nextTiles[a].distance < lowestDistance) {
                lowestDistance = nextTiles[a].distance;
            }
        }

        return lowestDistance;
    }

    List<NeighborTile> NextAvailable (NeighborTile startTile, GameObject hoverTile, List<GameObject> playerMoveTiles) {

        int startTileColumn = startTile.tile.GetComponent<TileActions>().columnIndex;
        int startTileRow = startTile.tile.GetComponent<TileActions>().rowIndex;
        int lowestDistance = GetCharacterMovement();
        List<NeighborTile> nextTiles = new List<NeighborTile> ();
        List<NeighborTile> paths = new List<NeighborTile> ();

        if (startTileColumn + 1 < InitializeGameElementsScript.ColumnLength) {
            GameObject upTile = InitializeGameElementsScript.Tiles[startTileColumn + 1, startTileRow];
            NeighborTile upTileNeighbor = new NeighborTile ();

            if (playerMoveTiles.Contains (upTile) && upTile != InitializeGameElementsScript.SelectedCharacterTile) {
                upTileNeighbor.tile = upTile;
                upTileNeighbor.distance = CalculateDistance (upTile, hoverTile);
                upTileNeighbor.cameFrom = startTile;
                nextTiles.Add (upTileNeighbor);
            }
        }

        if (startTileColumn - 1 >= 0) {
            GameObject downTile = InitializeGameElementsScript.Tiles[startTileColumn - 1, startTileRow];
            NeighborTile downTileNeighbor = new NeighborTile ();

            if (playerMoveTiles.Contains (downTile) && downTile != InitializeGameElementsScript.SelectedCharacterTile) {
                downTileNeighbor.tile = downTile;
                downTileNeighbor.distance = CalculateDistance (downTile, hoverTile);
                downTileNeighbor.cameFrom = startTile;
                nextTiles.Add (downTileNeighbor);
            }
        }

        if (startTileRow + 1 < InitializeGameElementsScript.RowLength) {
            GameObject rightTile = InitializeGameElementsScript.Tiles[startTileColumn, startTileRow + 1];
            NeighborTile rightTileNeighbor = new NeighborTile ();

            if (playerMoveTiles.Contains (rightTile) && rightTile != InitializeGameElementsScript.SelectedCharacterTile) {
                rightTileNeighbor.tile = rightTile;
                rightTileNeighbor.distance = CalculateDistance (rightTile, hoverTile);
                rightTileNeighbor.cameFrom = startTile;
                nextTiles.Add (rightTileNeighbor);
            }
        }

        if (startTileRow - 1 >= 0) {
            GameObject leftTile = InitializeGameElementsScript.Tiles[startTileColumn, startTileRow - 1];
            NeighborTile leftTileNeighbor = new NeighborTile ();

            if (playerMoveTiles.Contains (leftTile) && leftTile != InitializeGameElementsScript.SelectedCharacterTile) {
                leftTileNeighbor.tile = leftTile;
                leftTileNeighbor.distance = CalculateDistance (leftTile, hoverTile);
                leftTileNeighbor.cameFrom = startTile;
                nextTiles.Add (leftTileNeighbor);
            }
        }

        lowestDistance = FindLowestDistance (nextTiles, lowestDistance);

        for (int b = 0; b < nextTiles.Count; b++) {
            if (nextTiles[b].distance == lowestDistance) {
                paths.Add (nextTiles[b]);
            }
        }

        return nextTiles;
    }

    public void ResetAvailableTiles () {

        for (var i = 0; i < PlayerMoveTiles.Count; i++) {
            if (PlayerMoveTiles[i] != InitializeGameElementsScript.SelectedCharacterTile) {
                PlayerMoveTiles[i].GetComponent<MeshRenderer> ().material = AvailableTileMaterial;
            }
        }
    }

    void ConstructPath (NeighborTile current) {

        InitializeGameElementsScript.SelectedPath.Clear();

        NeighborTile traverseTile = current;
        traverseTile.tile.GetComponent<MeshRenderer>().material = HoverMaterial;
        InitializeGameElementsScript.SelectedPath.Add(traverseTile.tile);

        while(traverseTile.cameFrom != null) {
            traverseTile = traverseTile.cameFrom;

            InitializeGameElementsScript.SelectedPath.Add(traverseTile.tile);

            if(traverseTile.tile != InitializeGameElementsScript.SelectedCharacterTile) {
                traverseTile.tile.GetComponent<MeshRenderer>().material = HoverMaterial;
            }
        }

    }

    public void HighlightPath (List<GameObject> playerMoveTiles, GameObject hoverTile) {

        List<NeighborTile> openSet = new List<NeighborTile>();
        List<NeighborTile> cameFromSet = new List<NeighborTile>();
        List<GameObject> openSetTiles = new List<GameObject>();

        NeighborTile playerNeighborTile = new NeighborTile();
        playerNeighborTile.tile = InitializeGameElementsScript.SelectedCharacterTile;
        playerNeighborTile.distance = 0;
        playerNeighborTile.cameFrom = null;

        openSet.Add(playerNeighborTile);
        openSetTiles.Add(playerNeighborTile.tile);

        while (openSet.Count != 0) {

            NeighborTile current = openSet[0];

            if(current.tile == hoverTile) {
                ConstructPath(current);
                return;
            }

            openSet.RemoveAt(0);
            openSetTiles.RemoveAt(0);

            List<NeighborTile> newNeighbors = NextAvailable (current, hoverTile, playerMoveTiles);

            int lowestDistance =  GetCharacterMovement();

            for(int i = 0; i < newNeighbors.Count; i++){

                if(!openSetTiles.Contains(newNeighbors[i].tile)) {
                    openSet.Add(newNeighbors[i]);
                    openSetTiles.Add(newNeighbors[i].tile);
                }

                if(newNeighbors[i].distance < lowestDistance) {
                    lowestDistance = newNeighbors[i].distance;
                }
                
            }

            for(int k = 0; k < newNeighbors.Count; k++) {
                if(newNeighbors[k].distance == lowestDistance) {
                    current = newNeighbors[k];
                    break;
                }
            }
        }
    }

    public void DeselectTiles(GameObject oldPlayerTile)
    {
        for (int a = 0; a < PlayerMoveTiles.Count; a++)
        {
            PlayerMoveTiles[a].GetComponent<MeshRenderer>().material = DefaultTileMaterial;
        }

        oldPlayerTile.GetComponent<MeshRenderer>().material = DefaultTileMaterial;
        PlayerMoveTiles.Clear();
    }

    List<NeighborTile> GetNeighbors (NeighborTile startTile) {
		
        int startTileColumn = startTile.tile.GetComponent<TileActions>().columnIndex;
        int startTileRow = startTile.tile.GetComponent<TileActions>().rowIndex;
        List<NeighborTile> neighbors = new List<NeighborTile>();
        List<GameObject> characterTiles = new List<GameObject>();

        if (InitializeGameElementsScript.CurrentTurn == "player") {
            characterTiles = InitializeGameElementsScript.EnemyTiles;
        } else {
            characterTiles = InitializeGameElementsScript.PlayerCharacterTiles;
        }

        // Up
        if(startTileColumn + 1 < InitializeGameElementsScript.ColumnLength)
        {

            GameObject upTile = InitializeGameElementsScript.Tiles[startTileColumn + 1, startTileRow];
            NeighborTile upTileNeighbor = new NeighborTile();

            if (!InitializeGameElementsScript.ObstacleTiles.Contains(upTile) && 
			!characterTiles.Contains(upTile) && 
			startTile.distance < GetCharacterMovement() && 
			!ExcludedTiles.Contains(upTileNeighbor.tile))
            {
                upTileNeighbor.distance = startTile.distance + 1;
                upTileNeighbor.tile = upTile;
                neighbors.Add(upTileNeighbor);
            }

        }
        // Down
        if(startTileColumn - 1 >= 0)
        {

            GameObject downTile = InitializeGameElementsScript.Tiles[startTileColumn - 1, startTileRow];
            NeighborTile downTileNeighbor = new NeighborTile();

            if (!InitializeGameElementsScript.ObstacleTiles.Contains(downTile) && 
			!characterTiles.Contains(downTile) && 
			startTile.distance < GetCharacterMovement() && 
			!ExcludedTiles.Contains(downTileNeighbor.tile))
            {
                downTileNeighbor.distance = startTile.distance + 1;
                downTileNeighbor.tile = downTile;
                neighbors.Add(downTileNeighbor);
            }

        }
        // Right
        if(startTileRow + 1 < InitializeGameElementsScript.RowLength)
        {

            GameObject rightTile = InitializeGameElementsScript.Tiles[startTileColumn, startTileRow + 1];
            NeighborTile rightTileNeighbor = new NeighborTile();

            if (!InitializeGameElementsScript.ObstacleTiles.Contains(rightTile) && 
			!characterTiles.Contains(rightTile) && 
			startTile.distance < GetCharacterMovement() && 
			!ExcludedTiles.Contains(rightTileNeighbor.tile))
            {
                rightTileNeighbor.distance = startTile.distance + 1;
                rightTileNeighbor.tile = rightTile;
                neighbors.Add(rightTileNeighbor);
            }

        }
        // Left
        if(startTileRow - 1 >= 0)
        {

            GameObject leftTile = InitializeGameElementsScript.Tiles[startTileColumn, startTileRow - 1];
            NeighborTile leftTileNeighbor = new NeighborTile();

            if (!InitializeGameElementsScript.ObstacleTiles.Contains(leftTile) && 
			!characterTiles.Contains(leftTile) && 
			startTile.distance < GetCharacterMovement() && 
			!ExcludedTiles.Contains(leftTileNeighbor.tile))
            {
                leftTileNeighbor.distance = startTile.distance + 1;
                leftTileNeighbor.tile = leftTile;
                neighbors.Add(leftTileNeighbor);
            }

        }


        return neighbors;
    }

    public void GetMoveTiles (GameObject startingTile) {

        NeighborTile startingTileNeighbor = new NeighborTile();
        startingTileNeighbor.tile = startingTile;
        startingTileNeighbor.distance = 0;
        ExcludedTiles.Add(startingTile);
        List<NeighborTile> neighbors = GetNeighbors(startingTileNeighbor);
        bool keepIterating = true;

        while (keepIterating == true)
        {
            List<NeighborTile> newNeighbors = new List<NeighborTile>();

            for (int a = 0; a < neighbors.Count; a++)
            {

                if (!ExcludedTiles.Contains(neighbors[a].tile))
                {
                    ExcludedTiles.Add(neighbors[a].tile);
                }

                ExcludedTiles = ExcludedTiles.Distinct().ToList();

                if(neighbors[a].tile != InitializeGameElementsScript.SelectedCharacterTile)
                {
                    InitializeGameElementsScript.SetMaterial(AvailableTileMaterial, neighbors[a].tile);
                    PlayerMoveTiles.Add(neighbors[a].tile);
                }

                List <NeighborTile> addingNeighbors = GetNeighbors(neighbors[a]);

                for (int i = 0; i < addingNeighbors.Count; i++)
                {
                    List<NeighborTile> findMatch = newNeighbors.Where(b => b.tile == addingNeighbors[i].tile).ToList();

                    if(findMatch.Count == 0)
                    {
                        newNeighbors.Add(addingNeighbors[i]);
                    }
                }

                newNeighbors = newNeighbors.Distinct().ToList();

            }

            neighbors = newNeighbors;

            if(neighbors.Count == 0)
            {
                keepIterating = false;
            }
        }      

    }
}
