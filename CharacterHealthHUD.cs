﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHealthHUD : MonoBehaviour {

    private GlobalScript GlobalScript;
    private GameObject GlobalScriptObject;
    private InitializeGameElements InitializeGameElementsScript;
	
	public int CharacterHealth;
	public int CharacterMaxHealth;

	public Slider HealthSlider;

	void Awake () {
 		GameObject GlobalScriptObject = GameObject.FindGameObjectWithTag("GlobalScript");
        GlobalScript = GlobalScriptObject.GetComponent<GlobalScript>();
        InitializeGameElementsScript = GlobalScriptObject.GetComponentInChildren<InitializeGameElements>();
	}

	// Use this for initialization
	void Start () {

		UpdateHealthHUD();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void UpdateHealthHUD () {

		if (transform.parent.tag == "Enemy") {
			CharacterHealth = this.gameObject.GetComponentInParent<EnemyActions>().EnemyHealth;
			CharacterMaxHealth = this.gameObject.GetComponentInParent<EnemyActions>().EnemyTotalHealth;
		} else if (transform.parent.tag == "Player") {
			CharacterHealth = this.gameObject.GetComponentInParent<PlayerActions>().PlayerHealth;
			CharacterMaxHealth = this.gameObject.GetComponentInParent<PlayerActions>().PlayerTotalHealth;
		}

		this.gameObject.transform.Find("HealthSlider").GetComponent<Slider>().value = CharacterHealth;
		this.gameObject.transform.Find("HealthSlider").GetComponent<Slider>().maxValue = CharacterMaxHealth;
	}

}
