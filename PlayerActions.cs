﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour {

    private GlobalScript GlobalScript;
    private GameObject GlobalScriptObject;
    private InitializeGameElements InitializeGameElementsScript;
    private PlayerActions PlayerActionsScript;
    private TileOperations TileOperationsScript;
    private EnemyOperations EnemyOperationsScript; 
    private PlayerOperations PlayerOperationsScript;
    private PlayerActionHUD PlayerActionHUDScript;
    
    [HideInInspector] public int PlayerHealth = 100;
    [HideInInspector] public int PlayerTotalHealth = 100;
    [HideInInspector] public bool ActionCompleted = false;
    [HideInInspector] public bool Dead = false;

    public int Movement;
    public int PlayerMovementSpeed;
    public string Name;

    void Awake () {
        GameObject GlobalScriptObject = GameObject.FindGameObjectWithTag("GlobalScript");
        GameObject PlayerActionHUD = GameObject.FindGameObjectWithTag("PlayerActionHUD");
        GlobalScript = GlobalScriptObject.GetComponent<GlobalScript>();
        InitializeGameElementsScript = GlobalScriptObject.GetComponentInChildren<InitializeGameElements>();
        TileOperationsScript = GlobalScriptObject.GetComponentInChildren<TileOperations>();
        EnemyOperationsScript = GlobalScriptObject.GetComponentInChildren<EnemyOperations>();
        PlayerOperationsScript = GlobalScriptObject.GetComponentInChildren<PlayerOperations>();
        PlayerActionHUDScript = PlayerActionHUD.GetComponent<PlayerActionHUD>();
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayerDamage (int damage) {
        
        PlayerHealth = PlayerHealth - damage;
        this.gameObject.GetComponentInChildren<CharacterHealthHUD>().UpdateHealthHUD();
        CheckPlayerDead();

    }

    public void CheckPlayerDead () {

         if (PlayerHealth <= 0f) {
             
            this.gameObject.GetComponent<Renderer>().enabled = false;
            this.gameObject.GetComponentInChildren<CanvasGroup>().alpha = 0f;
            Dead = true;

            List<GameObject> PlayerCharacterList = new List<GameObject>(InitializeGameElementsScript.PlayerCharacters);
            PlayerCharacterList.Remove(this.gameObject);
            InitializeGameElementsScript.PlayerCharacters = PlayerCharacterList.ToArray();

            InitializeGameElementsScript.ResetPlayerCharacterTiles();

            if (PlayerOperationsScript.CheckAllPlayersDead()) {
                InitializeGameElementsScript.SetWinner("enemy");
            }
            
        }

    }

    void OnMouseUp ()
    {
        InitializeGameElementsScript.ActionMode = !InitializeGameElementsScript.ActionMode;

        if (InitializeGameElementsScript.ActionMode && 
        (InitializeGameElementsScript.CurrentTurn == "player") &&
        this.ActionCompleted == false)
        {
            InitializeGameElementsScript.SelectedCharacter = this.gameObject;
            InitializeGameElementsScript.SetSelectedPlayerTile();
            TileOperationsScript.GetMoveTiles(InitializeGameElementsScript.SelectedCharacterTile);
            InitializeGameElementsScript.SetMaterial(TileOperationsScript.PlayerTileMaterial, InitializeGameElementsScript.SelectedCharacterTile);
        } else
        {
            TileOperationsScript.DeselectTiles(InitializeGameElementsScript.SelectedCharacterTile);
        }

    }

    public void MovePlayer (GameObject moveTile)
    {

        InitializeGameElementsScript.PreviouslySelectedCharacterTile = InitializeGameElementsScript.SelectedCharacterTile;

        InitializeGameElementsScript.PlayerCharacterTiles.Remove(InitializeGameElementsScript.SelectedCharacterTile);

        InitializeGameElementsScript.SelectedCharacterTile = moveTile;

        int playerTileIndex = InitializeGameElementsScript.SelectedPath.IndexOf(InitializeGameElementsScript.SelectedCharacterTile);

        if(playerTileIndex > 0) {
            InitializeGameElementsScript.SelectedPath.RemoveAt(playerTileIndex);
        }

        InitializeGameElementsScript.SelectedPath.Reverse();

        StartCoroutine(MovePlayerOnPath()); 

        InitializeGameElementsScript.PlayerCharacterTiles.Add(InitializeGameElementsScript.SelectedCharacterTile);

    }

    public IEnumerator MovePlayerOnPath () {  

        for(int i = 0; i < InitializeGameElementsScript.SelectedPath.Count; i++) {

            Vector3 endPosition = new Vector3(InitializeGameElementsScript.SelectedPath[i].transform.position.x, 
            InitializeGameElementsScript.SelectedCharacter.transform.position.y, 
            InitializeGameElementsScript.SelectedPath[i].transform.position.z);
            
            while (InitializeGameElementsScript.SelectedCharacter.transform.position != endPosition)
            {
                InitializeGameElementsScript.SelectedCharacter.transform.position = Vector3.MoveTowards(
                InitializeGameElementsScript.SelectedCharacter.transform.position, 
                endPosition,
                PlayerMovementSpeed * Time.deltaTime);
                
                yield return null;
            }

        }

        PlayerActionHUDScript.ShowPlayerActionHUD();
    }

}
