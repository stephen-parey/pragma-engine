﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitializeGameElements : MonoBehaviour {

    private string Winner = "player";
    private bool GameOver = false;
    private List<CharacterHealthHUD> HealthHUDS;
    private GameObject[] Obstacles;

    [HideInInspector] public string CurrentTurn = "player";
    [HideInInspector] public bool ActionMode = false;
    [HideInInspector] public List<GameObject> PlayerCharacterTiles;
    [HideInInspector] public List<GameObject> ObstacleTiles;
    [HideInInspector] public List<GameObject> EnemyTiles;
    [HideInInspector] public List<GameObject> SelectedPath = new List<GameObject>();
    [HideInInspector] public GameObject[,] Tiles;
    [HideInInspector] public GameObject[] PlayerCharacters;
    [HideInInspector] public GameObject[] Enemies;
    [HideInInspector] public GameObject SelectedPlayerTile;
    [HideInInspector] public GameObject SelectedPlayer;
    [HideInInspector] public GameObject SelectedEnemy;
    [HideInInspector] public GameObject SelectedEnemyTile;
    [HideInInspector] public GameObject SelectedCharacter;
    [HideInInspector] public GameObject SelectedCharacterTile;
    [HideInInspector] public GameObject PreviouslySelectedCharacterTile;
    [HideInInspector] public GameObject PlayerTile;
    [HideInInspector] public GameObject ReferenceTile;

	public int ObstacleAmount;
	public int EnemyAmount;
    public int PlayerCharacterAmount;
    public int ColumnLength;
    public int RowLength;
    public Material ActionCompletedMaterial;
    public GameObject ReferencePlayer;
    public GameObject Player;
    public GameObject ReferenceEnemy;
    public GameObject ReferenceObstacle;
   

	void Start () {
		
	}

    public void InitializeGame () {
        Obstacles = new GameObject[ObstacleAmount];
        Enemies = new GameObject[EnemyAmount];
        PlayerCharacters = new GameObject[PlayerCharacterAmount];
        PlayerCharacterTiles = new List<GameObject>();
        ObstacleTiles = new List<GameObject>();
        EnemyTiles = new List<GameObject>();

        CreateTiles();
        CreatePlayerCharacters();
        CreateEnemies();
        CreateObstacles();
    }


    public GameObject InstantiateObjectOnTile (GameObject NewObject, GameObject Tile) {

        float tileX = Tile.transform.position.x;
        float tileZ = Tile.transform.position.z;

        Vector3 newObjectPosition = new Vector3(tileX, NewObject.transform.position.y, tileZ);

        return (GameObject)Instantiate(NewObject, newObjectPosition, NewObject.transform.rotation);

    }


    public GameObject FindRandomTile () {

        int randomRow = Random.Range(0, ColumnLength);
        int randomColumn = Random.Range(0, RowLength);

        return Tiles[randomColumn, randomRow];

    }

	public void CreateObstacles () {

        for(int i = 0; i < ObstacleAmount; i++) {

            GameObject randomTile = FindRandomTile();
            
            if(!PlayerCharacterTiles.Contains(randomTile) && !EnemyTiles.Contains(randomTile))
            {
                Obstacles[i] = InstantiateObjectOnTile(ReferenceObstacle, randomTile);
                ObstacleTiles.Add(randomTile);
            }
            
        }
    }

    public void CreateEnemies () {

        for(int i = 0; i < EnemyAmount; i++) {

            GameObject randomTile = FindRandomTile();

            if(!PlayerCharacterTiles.Contains(randomTile) && !ObstacleTiles.Contains(randomTile))
            {
                Enemies[i] = InstantiateObjectOnTile(ReferenceEnemy, randomTile);
                EnemyTiles.Add(randomTile);
            }
            
        }
    }

    public void CreateTiles () {
        Tiles = new GameObject[ColumnLength, RowLength];

        for (int i = 0; i < ColumnLength; i++) {

            for (int j = 0; j < RowLength; j++) {
                Tiles[i, j] = (GameObject)Instantiate(ReferenceTile, new Vector3(i, 0, j), ReferenceTile.transform.rotation);
                Tiles[i, j].GetComponent<TileActions>().columnIndex = i;
                Tiles[i, j].GetComponent<TileActions>().rowIndex = j;
            }
            
        }
    }

    public void CreatePlayerCharacters () {
        
        for(int i = 0; i < PlayerCharacterAmount; i++) {
        
            GameObject randomTile = FindRandomTile();

            PlayerCharacters[i] = InstantiateObjectOnTile(ReferencePlayer, randomTile);
            PlayerCharacterTiles.Add(randomTile);

        }

    }

    public void SetSelectedPlayerTile () {

        float SelectedCharacterPositionX = SelectedCharacter.transform.position.x;
        float SelectedCharacterPositionZ = SelectedCharacter.transform.position.z;

        List<GameObject> CharacterTiles = new List<GameObject>();

        if (SelectedCharacter.tag == "Player") {
            CharacterTiles = PlayerCharacterTiles;
        } else {
            CharacterTiles = EnemyTiles;
        }

        for (int i = 0; i < CharacterTiles.Count; i++) {

            if (CharacterTiles[i].transform.position.x == SelectedCharacterPositionX &&
            CharacterTiles[i].transform.position.z == SelectedCharacterPositionZ) {
                SelectedCharacterTile = CharacterTiles[i];
            }            

        }

    }

    public GameObject FindCorrespondingTile (GameObject character) {

        for (int i = 0; i < PlayerCharacterTiles.Count; i++) {

            if (PlayerCharacterTiles[i].transform.position.x == character.transform.position.x &&
            PlayerCharacterTiles[i].transform.position.z == character.transform.position.z) {
                return PlayerCharacterTiles[i];
            }

        }

        for (int i = 0; i < EnemyTiles.Count; i++) {

            if (EnemyTiles[i].transform.position.x == character.transform.position.x &&
            EnemyTiles[i].transform.position.z == character.transform.position.z) {
                return EnemyTiles[i];
            }

        }

        return null;

    }

    public GameObject FindMatchingTile (GameObject character) {

         for (int i = 0; i < ColumnLength; i++) {

            for (int j = 0; j < RowLength; j++) {

                if (Tiles[i, j].transform.position.x == character.transform.position.x &&
                Tiles[i, j].transform.position.z == character.transform.position.z) {
                    return Tiles[i, j];
                }

            }

        }

        return null;

    }

    public void SetWinner (string side) {

        Winner = side;
        GameOver = true;

        Debug.Log("Game Over");

        if (side == "player") {
            GameObject.FindGameObjectWithTag("MainUI").GetComponentInChildren<Text>().text = "Player Wins!";
        } else {
            GameObject.FindGameObjectWithTag("MainUI").GetComponentInChildren<Text>().text = "Enemy Wins";
        }
        
        GameObject.FindGameObjectWithTag("MainUI").GetComponent<CanvasGroup>().alpha = 1f;

    }

    public void ResetPlayerCharacterTiles () {
        
        PlayerCharacterTiles.Clear();

        for (int i = 0; i < PlayerCharacters.Length; i++) {

            PlayerCharacterTiles.Add(FindMatchingTile(PlayerCharacters[i]));

        }

    }

    public void ResetEnemyTiles () {
        
        EnemyTiles.Clear();

        for (int i = 0; i < Enemies.Length; i++) {

            EnemyTiles.Add(FindMatchingTile(Enemies[i]));

        }

    }

    public void SetMaterial(Material mat, GameObject objectToChange)
    {
        objectToChange.GetComponent<MeshRenderer>().material = mat;
    }
}
