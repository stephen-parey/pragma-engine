﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	private float rotationY = 0.0f;
	private float cameraY;

	public float translateSpeed = 6f;
	public float rotateSpeed = 80f;
	public float minFov = 15f;
	public float maxFov = 90f;
	public float sensitivity = 10f;

	void Awake () {
		cameraY = this.gameObject.transform.position.y;
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetMouseButton(0)) {
			RotateCamera();
		}

		ZoomCamera();
		
	}

	void FixedUpdate () {
		float h = Input.GetAxis ("Horizontal");
        float v = Input.GetAxis ("Vertical");

		MoveCamera(h, v);
	}

	void MoveCamera (float h, float v) {

		transform.localPosition += transform.right * h * translateSpeed * Time.deltaTime;
		transform.localPosition += transform.forward * v * translateSpeed * Time.deltaTime;

		Vector3 RIGHT = transform.TransformDirection(Vector3.right);
		Vector3 FORWARD = transform.TransformDirection(Vector3.forward);

		transform.localPosition += RIGHT * h * translateSpeed * Time.deltaTime;
		transform.localPosition += FORWARD * v * translateSpeed * Time.deltaTime;

		Vector3 newPosition = new Vector3 (transform.localPosition.x, cameraY, transform.localPosition.z);

		transform.localPosition = newPosition;
		 
    }

	void RotateCamera () {

		rotationY += Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime;
		transform.localEulerAngles = new Vector3(30, rotationY, 0);

	}

	void ZoomCamera () {

		float fov = Camera.main.fieldOfView;
		fov += Input.GetAxis("Mouse ScrollWheel") * sensitivity;
		fov = Mathf.Clamp(fov, minFov, maxFov);
		Camera.main.fieldOfView = fov;

	}
}
