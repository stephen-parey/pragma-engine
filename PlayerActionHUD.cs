﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionHUD : MonoBehaviour {

	private GlobalScript GlobalScript;
    private InitializeGameElements InitializeGameElementsScript;
	private PlayerOperations PlayerOperationsScript;
	private EnemyOperations EnemyOperationsScript;
	private GameObject GlobalScriptObject;
	private GameObject NearestEnemyTile;
	private GameObject NearestEnemy;

	// Use this for initialization
	void Start () {
		GameObject GlobalScriptObject = GameObject.FindGameObjectWithTag("GlobalScript");
        GlobalScript = GlobalScriptObject.GetComponent<GlobalScript>();
        InitializeGameElementsScript = GlobalScriptObject.GetComponentInChildren<InitializeGameElements>();
		PlayerOperationsScript = GlobalScriptObject.GetComponentInChildren<PlayerOperations>();
		EnemyOperationsScript = GlobalScriptObject.GetComponentInChildren<EnemyOperations>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

	}

	public void ShowPlayerActionHUD () {
		PlayerActionHUDMovement();
		this.GetComponent<CanvasGroup>().alpha = 1f;

		NearestEnemyTile = FindSurroundingEnemyTile();
		
		if (NearestEnemyTile) {
			NearestEnemy = FindSurroundingEnemy();
			ShowPlayerAttackOption();
		} else {
			HidePlayerAttackOption();
		}

	}

	public GameObject FindSurroundingEnemyTile () {

		int selectedRowIndex = InitializeGameElementsScript.SelectedCharacterTile.GetComponent<TileActions>().rowIndex;
		int selectedColumnIndex = InitializeGameElementsScript.SelectedCharacterTile.GetComponent<TileActions>().columnIndex;
		
		if (selectedRowIndex + 1 < InitializeGameElementsScript.RowLength) {
			GameObject upTile = InitializeGameElementsScript.Tiles[selectedColumnIndex, selectedRowIndex + 1];
			
			if (InitializeGameElementsScript.EnemyTiles.Contains(upTile)) {
				return upTile;
			}
		} 
		if (selectedRowIndex - 1 >= 0) {
			GameObject downTile = InitializeGameElementsScript.Tiles[selectedColumnIndex, selectedRowIndex - 1];

			if (InitializeGameElementsScript.EnemyTiles.Contains(downTile)) {
				return downTile;
			}
		} 
		if (selectedColumnIndex + 1 < InitializeGameElementsScript.ColumnLength) {
			GameObject rightTile = InitializeGameElementsScript.Tiles[selectedColumnIndex + 1, selectedRowIndex];

			if (InitializeGameElementsScript.EnemyTiles.Contains(rightTile)) {
				return rightTile;
			}
		} 
		if (selectedColumnIndex - 1 >= 0) {
			GameObject leftTile = InitializeGameElementsScript.Tiles[selectedColumnIndex - 1, selectedRowIndex];

			if (InitializeGameElementsScript.EnemyTiles.Contains(leftTile)) {
				return leftTile;
			}
		}

		return null;

	}

	public GameObject FindSurroundingEnemy () {

		for (int i = 0; i < InitializeGameElementsScript.Enemies.Length; i++) {

			if (InitializeGameElementsScript.Enemies[i].transform.position.x == NearestEnemyTile.transform.position.x &&
			InitializeGameElementsScript.Enemies[i].transform.position.z == NearestEnemyTile.transform.position.z) {
				return InitializeGameElementsScript.Enemies[i];
			}

		}

		return null;

	}

	public void EndCurrentPlayerTurn () {

		InitializeGameElementsScript.SelectedCharacter.GetComponent<PlayerActions>().ActionCompleted = true;
        InitializeGameElementsScript.SetMaterial(InitializeGameElementsScript.ActionCompletedMaterial, InitializeGameElementsScript.SelectedCharacter);

		InitializeGameElementsScript.ResetPlayerCharacterTiles();

        if (PlayerOperationsScript.IsPlayerTurnComplete()) {
            InitializeGameElementsScript.CurrentTurn = "enemy";
            EnemyOperationsScript.InitiateEnemyPhase();
        }

	}

	public void PlayerWait () {

		EndCurrentPlayerTurn();
		HidePlayerActionHUD();

	}

	public void ShowPlayerAttackOption () {
		GameObject AttackButton = GameObject.FindGameObjectWithTag("AttackButton");
		AttackButton.GetComponent<CanvasGroup>().alpha = 1f;
	}

	public void HidePlayerAttackOption () {
		GameObject AttackButton = GameObject.FindGameObjectWithTag("AttackButton");
		AttackButton.GetComponent<CanvasGroup>().alpha = 0f;
	}

	public void PlayerAttack () {
		NearestEnemy.GetComponent<EnemyActions>().EnemyDamage(25);

		EndCurrentPlayerTurn();
		HidePlayerActionHUD();
	}

	public void CancelPlayerMove () {

		InitializeGameElementsScript.PlayerCharacterTiles.Remove(InitializeGameElementsScript.SelectedCharacterTile);

        InitializeGameElementsScript.SelectedCharacterTile = InitializeGameElementsScript.PreviouslySelectedCharacterTile;

		InitializeGameElementsScript.PlayerCharacterTiles.Add(InitializeGameElementsScript.SelectedCharacterTile);

		InitializeGameElementsScript.SelectedCharacter.transform.position = new Vector3(
		InitializeGameElementsScript.SelectedCharacterTile.transform.position.x,
		InitializeGameElementsScript.SelectedCharacter.transform.position.y, 
		InitializeGameElementsScript.SelectedCharacterTile.transform.position.z);

		HidePlayerActionHUD();

	}

	public void HidePlayerActionHUD () {
		this.GetComponent<CanvasGroup>().alpha = 0f;
	}

	private void PlayerActionHUDMovement () {

		if(InitializeGameElementsScript.SelectedCharacter && InitializeGameElementsScript.SelectedCharacter.tag == "Player") {
			Vector3 newPosition = new Vector3 (InitializeGameElementsScript.SelectedCharacter.transform.position.x, 
			(this.transform.localScale.y + 0.6f), 
			InitializeGameElementsScript.SelectedCharacter.transform.position.z);

			this.transform.position = newPosition;
		}
		
	}
}
