﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainUI : MonoBehaviour {

	private Transform MainMenu;

	void Awake () {
		MainMenu = this.gameObject.transform.Find("MainMenu");
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowMainMenu () {
		MainMenu.GetComponent<CanvasGroup>().alpha = 1f;
		MainMenu.GetComponent<CanvasGroup>().interactable = true;
	}

	public void HideMainMenu () {
		MainMenu.GetComponent<CanvasGroup>().alpha = 0f;
		MainMenu.GetComponent<CanvasGroup>().interactable = false;
	}
}
