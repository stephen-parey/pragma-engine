﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalScript : MonoBehaviour {

	void Awake() {
		this.gameObject.GetComponentInChildren<InitializeGameElements>().InitializeGame();
	} 

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
