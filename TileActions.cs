﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileActions : MonoBehaviour {

    private GameObject GlobalScriptObject;
    private GlobalScript GlobalScript;
    private InitializeGameElements InitializeGameElementsScript;
    private TileOperations TileOperationsScript;
    private EnemyOperations EnemyOperationsScript;

    public int columnIndex;
    public int rowIndex;

    void Awake () {
        GlobalScriptObject = GameObject.FindGameObjectWithTag("GlobalScript");
        TileOperationsScript = GlobalScriptObject.GetComponentInChildren<TileOperations>();
        InitializeGameElementsScript = GlobalScriptObject.GetComponentInChildren<InitializeGameElements>();
        EnemyOperationsScript = GlobalScriptObject.GetComponentInChildren<EnemyOperations>();
    }

    // Use this for initialization
    void Start () {

    }
    // Update is called once per frame
    void Update () {

    }

    void OnMouseUp () {
      
        GameObject oldTile = InitializeGameElementsScript.SelectedCharacterTile;

        if (TileOperationsScript.PlayerMoveTiles.Contains(this.gameObject) && 
        InitializeGameElementsScript.ActionMode && 
        !InitializeGameElementsScript.PlayerCharacterTiles.Contains(this.gameObject)) {

            TileOperationsScript.DeselectTiles(oldTile);
            InitializeGameElementsScript.ActionMode = false;

            if (InitializeGameElementsScript.SelectedCharacter.tag == "Player") {
                InitializeGameElementsScript.SelectedCharacter.GetComponent<PlayerActions>().MovePlayer(this.gameObject);
            } else {
                InitializeGameElementsScript.SelectedCharacter.GetComponent<EnemyActions>().MoveEnemy(this.gameObject);
            }

        }

    }

    void OnMouseOver () {

        TileOperationsScript.ResetAvailableTiles();

        if (TileOperationsScript.PlayerMoveTiles.Contains(this.gameObject) && 
        !InitializeGameElementsScript.PlayerCharacterTiles.Contains(this.gameObject) &&
        InitializeGameElementsScript.ActionMode) {
              TileOperationsScript.HighlightPath(TileOperationsScript.PlayerMoveTiles, this.gameObject);
        }

    }
}