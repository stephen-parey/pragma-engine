﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOperations : MonoBehaviour {

	private bool IsMoving = false;
	private int EnemyMovementSpeed;
	private GlobalScript GlobalScript;
	private GameObject NearestPlayerTile;
	private GameObject NearestPlayer;
	private GameObject SurroundingPlayerTile;
	private GameObject SurroundingPlayer;
	private GameObject GlobalScriptObject;
	private InitializeGameElements InitializeGameElementsScript;
	private TileOperations TileOperationsScript;
	private PlayerOperations PlayerOperationsScript;
	
	public Material EnemyMaterial;

	void Awake () {
		GameObject GlobalScriptObject = GameObject.FindGameObjectWithTag("GlobalScript");
		GlobalScript = GlobalScriptObject.GetComponent<GlobalScript>();
        InitializeGameElementsScript = GlobalScriptObject.GetComponentInChildren<InitializeGameElements>();
		TileOperationsScript = GlobalScriptObject.GetComponentInChildren<TileOperations>();
		PlayerOperationsScript = GlobalScriptObject.GetComponentInChildren<PlayerOperations>();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		IsMoving = IsEnemyMoving();
	}

	GameObject FindClosestPlayer () {

		float lowestPlayerDistance = 10000000000;
		
		for (int i = 0; i < InitializeGameElementsScript.PlayerCharacters.Length; i++) {
			
			float distance = Vector3.Distance(InitializeGameElementsScript.PlayerCharacters[i].transform.position,
			InitializeGameElementsScript.SelectedCharacter.transform.position);

			if (distance < lowestPlayerDistance && !InitializeGameElementsScript.PlayerCharacters[i].GetComponent<PlayerActions>().Dead) {
				lowestPlayerDistance = distance;
				NearestPlayer = InitializeGameElementsScript.PlayerCharacters[i];
			}

		}

		return NearestPlayer;

	}

	GameObject FindClosestMoveTile (GameObject closestPlayer) {

		float lowestTileDistance = 1000000;

		for (int j = 0; j < TileOperationsScript.PlayerMoveTiles.Count; j++) {

			float tileDistance = Vector3.Distance(TileOperationsScript.PlayerMoveTiles[j].transform.position,
			closestPlayer.transform.position);

			if (tileDistance < lowestTileDistance && !InitializeGameElementsScript.EnemyTiles.Contains(TileOperationsScript.PlayerMoveTiles[j])) {
				lowestTileDistance = tileDistance;
				NearestPlayerTile = TileOperationsScript.PlayerMoveTiles[j];
			}

		}


		return NearestPlayerTile;
	}


	public bool IsEnemyMoving () {

		if(InitializeGameElementsScript.Enemies.Length > 0) {

			for (int i = 0; i < InitializeGameElementsScript.Enemies.Length; i++) {
				if(InitializeGameElementsScript.Enemies[i] && InitializeGameElementsScript.Enemies[i].gameObject.GetComponent<EnemyActions>().IsMoving == true) {
					return true;
				}
			}

		}

		return false;
	}


	public GameObject GetEnemyMoveTile () {

		
		TileOperationsScript.GetMoveTiles(InitializeGameElementsScript.SelectedCharacterTile);
		
		GameObject closestPlayer = FindClosestPlayer();
		GameObject closestMoveTile = FindClosestMoveTile(closestPlayer);

		TileOperationsScript.HighlightPath(TileOperationsScript.PlayerMoveTiles, closestMoveTile);
		TileOperationsScript.DeselectTiles(closestMoveTile);

		return closestMoveTile;


	}

	public bool CheckEnemyActionCompleted () {
		
		for (int i = 0; i < InitializeGameElementsScript.Enemies.Length; i++) {

			if (!InitializeGameElementsScript.Enemies[i].GetComponent<EnemyActions>().ActionCompleted) {
				return false;
			}

		}

		return true;
	}

	public GameObject FindSurroundingPlayerTile () {

		int selectedRowIndex = InitializeGameElementsScript.SelectedCharacterTile.GetComponent<TileActions>().rowIndex;
		int selectedColumnIndex = InitializeGameElementsScript.SelectedCharacterTile.GetComponent<TileActions>().columnIndex;
		
		if (selectedRowIndex + 1 < InitializeGameElementsScript.RowLength) {
			GameObject upTile = InitializeGameElementsScript.Tiles[selectedColumnIndex, selectedRowIndex + 1];
			
			if (InitializeGameElementsScript.PlayerCharacterTiles.Contains(upTile)) {
				return upTile;
			}
		} 
		if (selectedRowIndex - 1 >= 0) {
			GameObject downTile = InitializeGameElementsScript.Tiles[selectedColumnIndex, selectedRowIndex - 1];

			if (InitializeGameElementsScript.PlayerCharacterTiles.Contains(downTile)) {
				return downTile;
			}
		} 
		if (selectedColumnIndex + 1 < InitializeGameElementsScript.ColumnLength) {
			GameObject rightTile = InitializeGameElementsScript.Tiles[selectedColumnIndex + 1, selectedRowIndex];

			if (InitializeGameElementsScript.PlayerCharacterTiles.Contains(rightTile)) {
				return rightTile;
			}
		} 
		if (selectedColumnIndex - 1 >= 0) {
			GameObject leftTile = InitializeGameElementsScript.Tiles[selectedColumnIndex - 1, selectedRowIndex];

			if (InitializeGameElementsScript.PlayerCharacterTiles.Contains(leftTile)) {
				return leftTile;
			}
		}

		return null;
		
	}

	public GameObject FindSurroundingPlayer () {

		for (int i = 0; i < InitializeGameElementsScript.PlayerCharacters.Length; i++) {

			if (InitializeGameElementsScript.PlayerCharacters[i].transform.position.x == SurroundingPlayerTile.transform.position.x &&
			InitializeGameElementsScript.PlayerCharacters[i].transform.position.z == SurroundingPlayerTile.transform.position.z) {
				return InitializeGameElementsScript.PlayerCharacters[i];
			}

		}

		return null;

	}

	public bool CheckForSurroundingPlayers () {
		SurroundingPlayerTile = FindSurroundingPlayerTile();

		if (SurroundingPlayerTile) {
			SurroundingPlayer = FindSurroundingPlayer();
			return true;
		}

		return false;
	}

	public void EnemyAttackAction() {
		SurroundingPlayer.GetComponent<PlayerActions>().PlayerDamage(10);

		InitializeGameElementsScript.SelectedCharacter.GetComponent<EnemyActions>().ActionCompleted = true;
        InitializeGameElementsScript.SetMaterial(InitializeGameElementsScript.ActionCompletedMaterial, InitializeGameElementsScript.SelectedCharacter);
        this.IsMoving = false;

		if (!CheckEnemyActionCompleted()) {
            SetEnemyAction();
        } else {
            PlayerOperationsScript.SetPlayerTurn();
        }

	}

	public void EnemyAI () {
		
		if (CheckForSurroundingPlayers()) {
			EnemyAttackAction();
		} else {
			EnemyMoveAction();
		}

	}

	public void EnemyMoveAction () {
		InitializeGameElementsScript.SelectedCharacter.GetComponent<EnemyActions>().MoveEnemy(GetEnemyMoveTile());
	}

	public void SetEnemyAction () {

		for (int i = 0; i < InitializeGameElementsScript.Enemies.Length; i++) {

			if (!InitializeGameElementsScript.Enemies[i].GetComponent<EnemyActions>().ActionCompleted) {
				InitializeGameElementsScript.SelectedCharacter = InitializeGameElementsScript.Enemies[i];
				InitializeGameElementsScript.SetSelectedPlayerTile();

				EnemyAI();

				break;
			}

		}
	}

	public bool CheckAllEnemiesDead () {

		for (int i = 0; i < InitializeGameElementsScript.Enemies.Length; i++) {

			if (!InitializeGameElementsScript.Enemies[i].GetComponent<EnemyActions>().Dead) {
				return false;
			}

		}

		return true;

	}


	public void InitiateEnemyPhase () {

		SetEnemyAction();

	}

}
