﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOperations : MonoBehaviour {

	private GlobalScript GlobalScript;
    private GameObject GlobalScriptObject;
    private InitializeGameElements InitializeGameElementsScript;
    private PlayerActions PlayerActionsScript;
    private TileOperations TileOperationsScript;
    private EnemyOperations EnemyOperationsScript; 

	public Material PlayerCharacterMaterial;
	
	void Awake () {
        GameObject GlobalScriptObject = GameObject.FindGameObjectWithTag("GlobalScript");
        GlobalScript = GlobalScriptObject.GetComponent<GlobalScript>();
        InitializeGameElementsScript = GlobalScriptObject.GetComponentInChildren<InitializeGameElements>();
        TileOperationsScript = GlobalScriptObject.GetComponentInChildren<TileOperations>();
        EnemyOperationsScript = GlobalScriptObject.GetComponentInChildren<EnemyOperations>();
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public bool CheckAllPlayersDead () {

		for (int i = 0; i < InitializeGameElementsScript.PlayerCharacters.Length; i++) {

			if (!InitializeGameElementsScript.PlayerCharacters[i].GetComponent<PlayerActions>().Dead) {
				return false;
			}

		}

		return true;

	}

	public void SetPlayerTurn () {
		
		for(int i = 0; i < InitializeGameElementsScript.PlayerCharacters.Length; i++) {

			InitializeGameElementsScript.SetMaterial(PlayerCharacterMaterial, InitializeGameElementsScript.PlayerCharacters[i]);
			InitializeGameElementsScript.PlayerCharacters[i].GetComponent<PlayerActions>().ActionCompleted = false;

		}

		for(int j = 0; j < InitializeGameElementsScript.Enemies.Length; j++) {

			InitializeGameElementsScript.SetMaterial(EnemyOperationsScript.EnemyMaterial, InitializeGameElementsScript.Enemies[j]);
			InitializeGameElementsScript.Enemies[j].GetComponent<EnemyActions>().ActionCompleted = false;

		}

    	InitializeGameElementsScript.CurrentTurn = "player";
	}

	public void EndPlayerTurn () {

		for(int i = 0; i < InitializeGameElementsScript.PlayerCharacters.Length; i++) {

			InitializeGameElementsScript.SetMaterial(InitializeGameElementsScript.ActionCompletedMaterial, InitializeGameElementsScript.PlayerCharacters[i]);
			InitializeGameElementsScript.PlayerCharacters[i].GetComponent<PlayerActions>().ActionCompleted = true;

		}

		for(int j = 0; j < InitializeGameElementsScript.Enemies.Length; j++) {

			InitializeGameElementsScript.SetMaterial(InitializeGameElementsScript.ActionCompletedMaterial, InitializeGameElementsScript.Enemies[j]);
			InitializeGameElementsScript.Enemies[j].GetComponent<EnemyActions>().ActionCompleted = false;

		}

		InitializeGameElementsScript.CurrentTurn = "enemy";
		EnemyOperationsScript.InitiateEnemyPhase();

	}

	public bool IsPlayerTurnComplete () {

		for (int i = 0; i < InitializeGameElementsScript.PlayerCharacters.Length; i++) {
			if (InitializeGameElementsScript.PlayerCharacters[i].GetComponent<PlayerActions>().ActionCompleted == false) {
				return false;
			}
		}

		return true;

	}
}
