﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActions : MonoBehaviour {

    private GlobalScript GlobalScript;
	private GameObject GlobalScriptObject;
    private EnemyOperations EnemyOperationsScript;
    private PlayerOperations PlayerOperationsScript;
	private InitializeGameElements InitializeGameElementsScript;

    [HideInInspector] public int EnemyHealth = 50;
    [HideInInspector] public int EnemyTotalHealth = 110;
    [HideInInspector] public bool ActionCompleted = false;
    [HideInInspector] public bool IsMoving = false;
    [HideInInspector] public bool Dead = false;

	public int Movement;
	public int EnemyMovementSpeed;

	void Awake () {
		GameObject GlobalScriptObject = GameObject.FindGameObjectWithTag("GlobalScript");
		GlobalScript = GlobalScriptObject.GetComponent<GlobalScript>();
        InitializeGameElementsScript = GlobalScriptObject.GetComponentInChildren<InitializeGameElements>();
        EnemyOperationsScript = GlobalScriptObject.GetComponentInChildren<EnemyOperations>();
        PlayerOperationsScript = GlobalScriptObject.GetComponentInChildren<PlayerOperations>();
        EnemyHealth = 50;
        EnemyTotalHealth = 110;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void EnemyDamage (int damage) {
        EnemyHealth = EnemyHealth - damage;
        this.gameObject.GetComponentInChildren<CharacterHealthHUD>().UpdateHealthHUD();
        CheckEnemyDead();
    }

    public void CheckEnemyDead () {

        if (EnemyHealth <= 0f) {
            this.gameObject.GetComponent<Renderer>().enabled = false;
            this.gameObject.transform.Find("CharacterHealthHUD").GetComponent<CanvasGroup>().alpha = 0f;
            Dead = true;

            List<GameObject> EnemyList = new List<GameObject>(InitializeGameElementsScript.Enemies);
            EnemyList.Remove(this.gameObject);
            InitializeGameElementsScript.Enemies = EnemyList.ToArray();

            InitializeGameElementsScript.ResetEnemyTiles();

        }

        if (EnemyOperationsScript.CheckAllEnemiesDead()) {
            InitializeGameElementsScript.SetWinner("player");
        }

    }

	public void MoveEnemy (GameObject moveTile)
    {

        InitializeGameElementsScript.EnemyTiles.Remove(InitializeGameElementsScript.SelectedCharacterTile);

        InitializeGameElementsScript.SelectedCharacterTile = moveTile;

        int enemyTileIndex = InitializeGameElementsScript.SelectedPath.IndexOf(InitializeGameElementsScript.SelectedCharacterTile);
        
        if(enemyTileIndex > 0) {
            InitializeGameElementsScript.SelectedPath.RemoveAt(enemyTileIndex);
        }

        InitializeGameElementsScript.SelectedPath.Reverse();

        StartCoroutine(MoveEnemyOnPath()); 

        InitializeGameElementsScript.EnemyTiles.Add(InitializeGameElementsScript.SelectedCharacterTile);
    }

	public IEnumerator MoveEnemyOnPath () {  
        this.IsMoving = true;

        for(int i = 0; i < InitializeGameElementsScript.SelectedPath.Count; i++) {

            Vector3 endPosition = new Vector3(InitializeGameElementsScript.SelectedPath[i].transform.position.x, 
            InitializeGameElementsScript.SelectedCharacter.transform.position.y, 
            InitializeGameElementsScript.SelectedPath[i].transform.position.z);
            
            while (InitializeGameElementsScript.SelectedCharacter.transform.position != endPosition)
            {
                InitializeGameElementsScript.SelectedCharacter.transform.position = Vector3.MoveTowards(
                InitializeGameElementsScript.SelectedCharacter.transform.position,
                endPosition, 
                EnemyMovementSpeed * Time.deltaTime);
                yield return null;
            }

        }

        this.IsMoving = false;

        if (EnemyOperationsScript.CheckForSurroundingPlayers()) {
            EnemyOperationsScript.EnemyAttackAction();
        } else {
            InitializeGameElementsScript.SelectedCharacter.GetComponent<EnemyActions>().ActionCompleted = true;
            InitializeGameElementsScript.SetMaterial(InitializeGameElementsScript.ActionCompletedMaterial, InitializeGameElementsScript.SelectedCharacter);

            if (!EnemyOperationsScript.CheckEnemyActionCompleted()) {
                EnemyOperationsScript.SetEnemyAction();
            } else {
                PlayerOperationsScript.SetPlayerTurn();
            }
        }

    }
}
